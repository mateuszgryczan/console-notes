﻿using System;
using System.Collections.Generic;

namespace ConsoleNotes
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MenuActionService menuActionService = new MenuActionService();
            menuActionService = Initialize(menuActionService);

            NoteService noteService = new NoteService();

            Console.WriteLine("Welcome to the ConsoleNotes app!");

            while (true)
            {
                List<MenuAction> mainMenu = menuActionService.GetMenuActionsByMenuName("Main");

                foreach (MenuAction action in mainMenu)
                {
                    Console.WriteLine($"{action.Id}. {action.Content}");
                }

                Console.Write("\nOption: ");
                ConsoleKeyInfo operation = Console.ReadKey();

                switch (operation.KeyChar)
                {
                    case '1':
                        Console.Write("\n");
                        Note addObj = noteService.AddNoteView();
                        noteService.AddNote(addObj);
                        break;
                    case '2':
                        int deleteId = noteService.DeleteNoteByIdView();
                        noteService.DeleteNoteById(deleteId);
                        break;
                    case '3':
                        Console.Write("\n");
                        ConsoleKeyInfo keyInfo = noteService.NotesView(menuActionService);
                        
                        switch(keyInfo.KeyChar)
                        {
                            case '1':
                                int searchId = noteService.SearchNoteByIdView();
                                noteService.SearchNoteById(searchId);
                                break;
                            case '2':
                                string title = noteService.SearchNoteByTitleView();
                                noteService.SearchNoteByTitle(title);
                                break;
                            case '3':
                                noteService.ShowAllNotes();
                                break;
                        }

                        break;
                    default:
                        Console.Write("\n\nInvalid operation..");
                        break;
                }
                Console.WriteLine("\n");
            }

        }

        private static MenuActionService Initialize(MenuActionService menuActionService)
        {
            menuActionService.AddNewAction(1, "Add note", "Main");
            menuActionService.AddNewAction(2, "Delete note", "Main");
            menuActionService.AddNewAction(3, "Notes", "Main");

            menuActionService.AddNewAction(1, "Search by id", "Notes");
            menuActionService.AddNewAction(2, "Search by title", "Notes");
            menuActionService.AddNewAction(3, "Show all notes", "Notes");

            return menuActionService;
        }
    }
}
