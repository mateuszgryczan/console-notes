﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleNotes
{
    public class Note
    {
        public int Id;
        public string Title;
        public string Content;
    }
}
