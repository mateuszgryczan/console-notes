﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleNotes
{
    public class MenuAction
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string MenuName { get; set; }
    }
}
