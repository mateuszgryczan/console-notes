﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleNotes
{
    public class NoteService
    {
        public List<Note> Notes { get; set; }

        public NoteService()
        {
            Notes = new List<Note>();
        }

        public Note AddNoteView()
        {
            Console.Write("Enter id: ");
            string id = Console.ReadLine();
            int noteId;
            Int32.TryParse(id, out noteId);

            Console.Write("Enter title: ");
            string noteTitle = Console.ReadLine();

            Console.Write("Enter content: ");
            string noteContent = Console.ReadLine();

            Note note = new Note() { Id = noteId, Title = noteTitle, Content = noteContent };

            return note;
        }

        public void AddNote(Note note)
        {
            Notes.Add(note);

            Console.Write($"\nThe \"{note.Title}\" note has been successfully added.");
        }

        public void ShowAllNotes()
        {
            if (Notes.Count > 0)
            {
                foreach (Note note in Notes)
                {
                    Console.Write($"\n\n{note.Id}. {note.Title} - {note.Content}");
                }
            }
            else
            {
                Console.Write("\n\nThe list of notes is empty, please add some notes.");
            }
        }

        public int DeleteNoteByIdView()
        {
            Console.Write("\nId of note you want to remove: ");
            string output = Console.ReadLine();
            int noteId;
            Int32.TryParse(output, out noteId);

            return noteId;
        }

        public void DeleteNoteById(int noteid)
        {
            Note noteToDelete = new Note();

            foreach (var note in Notes)
            {
                if (note.Id == noteid)
                {
                    noteToDelete = note;
                }
            }

            Console.Write($"\nThe \"{noteToDelete.Title}\" note is successfully deleted.");

            Notes.Remove(noteToDelete);
        }

        public String SearchNoteByTitleView()
        {
            Console.Write("\nThe title of note you want to search for: ");
            string title = Console.ReadLine();

            return title;
        }

        public void SearchNoteByTitle(String title)
        {
            foreach (Note note in Notes)
            {
                if (String.Equals(note.Title, title))
                {
                    Console.Write($"\n{note.Id}. {note.Title} - {note.Content}");
                }
            }
        }

        public int SearchNoteByIdView()
        {
            Console.Write("\nThe id of note you want to search for: ");
            string output = Console.ReadLine();
            int noteId;
            Int32.TryParse(output, out noteId);

            return noteId;
        }

        public void SearchNoteById(int id)
        {
            foreach (Note note in Notes)
            {
                if(note.Id == id)
                {
                    Console.Write($"\n{note.Id}. {note.Title} - {note.Content}");
                }
            }
        }

        public ConsoleKeyInfo NotesView(MenuActionService menuActionService)
        {
            List<MenuAction> notes = menuActionService.GetMenuActionsByMenuName("Notes");

            for (int i = 0; i < notes.Count; i++)
            {
                Console.Write($"\n{notes[i].Id}. {notes[i].Content}");
            }

            Console.Write("\n\nOption: ");
            ConsoleKeyInfo operation = Console.ReadKey();

            return operation;
        }
    }
}
