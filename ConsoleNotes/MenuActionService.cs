﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleNotes
{
    public class MenuActionService
    {
        private List<MenuAction> menuActions;
        public MenuActionService()
        {
            menuActions = new List<MenuAction>();
        }

        public List<MenuAction> GetMenuActionsByMenuName(string menuName)
        {
            List<MenuAction> result = new List<MenuAction>();

            foreach(MenuAction action in menuActions)
            {
                if(action.MenuName == menuName)
                {
                    result.Add(action);
                }
            }

            return result;
        }

        public void AddNewAction(int id, string content, string menuName)
        {
            MenuAction menuAction = new MenuAction() { Id = id, Content = content, MenuName = menuName };
            menuActions.Add(menuAction);
        }
    }
}
